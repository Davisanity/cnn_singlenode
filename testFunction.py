import tensorflow as tf
import os
import matplotlib.pyplot as plt
import numpy as np
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
from PIL import Image
import random

# def getOneHot(data):
# 	catogories=3
# 	y=np.zeros([len(data),catogories])
# 	for i in range(len(data)):
# 		if data[i].find('shapeW')!=-1:
# 			y[i][0]=1
# 		elif data[i].find('shapeV')!=-1:
# 			y[i][1]=1
# 		else:
# 			y[i][2]=1
# 	print(y)
# 	return y

# def getOneHotLabel(label, depth):
#     m = np.zeros([len(label), depth])
#     for i in range(len(label)):
#         m[i][label[i]] = 1
#     return m

# def batch(x,y):
# 	tfpath=tf.constant(x)
# 	tflabel=tf.constant(y)
# 	batch = tf.train.shuffle_batch([tfpath,tflabel],enqueue_many=True, batch_size=3, capacity=2, min_after_dequeue=1, allow_smaller_final_batch=True)
# 	# REF: https://www.tensorflow.org/api_docs/python/tf/train/shuffle_batch
# 	return batch

def CNN(image,keepprob=0.5):
	print("image shape: ",image.shape)
	parameters = []
	kernel = tf.Variable(tf.truncated_normal([5, 5, 1, 64], dtype=tf.float32, stddev=1e-1, name="weights"))
	print(kernel)
	conv = tf.nn.conv2d(image, kernel, [1, 5, 5, 1], padding="SAME")
	biases = tf.Variable(tf.constant(0.0, dtype=tf.float32, shape=[64]), trainable=True, name="biases")
	bias = tf.nn.bias_add(conv, biases)
	conv1 = tf.nn.relu(bias, name="conv1")

	pool1 = tf.nn.max_pool(conv1,ksize=[1,3,3,1],strides=[1,2,2,1],padding="VALID",name="pool1")

	flatten1 = tf.reshape(pool1,[-1,2*2*64])
	weight1 = tf.Variable(tf.truncated_normal([2*2*64,3], dtype=tf.float32, stddev=1e-1, name="weights"))
	fc1 = tf.nn.relu(tf.matmul(flatten1,weight1)) 
	print(fc1)
	dropout1=tf.nn.dropout(fc1,keepprob)
	print(dropout1)
	return dropout1

def mul(l):
	# x = tf.Variable(l,name='x',dtype=tf.float32)
	with tf.name_scope("heyhey") as scope:
		# a = tf.Variable(2.0)
		# b = tf.Variable(3.0)
		# c = tf.Variable(tf.add(a,b))
		y = tf.scalar_mul(2.0,l)
	return y


if __name__ == "__main__":
	# data = ['data\shapeV\16181.png','data\other\16255.png',
	# 		'data\shapeV\17202.png','data\shapeW\74556.png',
	# 		'data\other\74678.png']
	# getOneHot(data)
	# x =['a','b','C','D','E','f','g','h','i','j','k','l','m','n','o','p']
	# y =['a','b','C','D','E','f','g','h','i','j','k','l','m','n','o','p']
	
	a =[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
	# b =[0.5,1.2,2.4,4.1,5.2,6.4,7.5,8.1,9.2,10.1,11.3,12,13,14.1,15,16.2]
	tfinput = tf.placeholder(dtype=tf.float32, shape=[None], name='image')
	# tfoutput = tf.placeholder(dtype=tf.float32, shape=[None], name='image')
	# tflabel = tf.placeholder(dtype=tf.float32, shape=[None], name='label')
	# x_batch,y_batch=tf.train.shuffle_batch([a,b],enqueue_many=True, batch_size=3, capacity=100, min_after_dequeue=10, allow_smaller_final_batch=True)
	
	# print(tf.cast(x_batch,tf.float32))
	# o = mul(tf.cast(x_batch,tf.float32))
	o = mul(tfinput)/16
	# print(o)
	# loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=tfoutput, labels=tflabel)) #這裡是關鍵!!!!! 和原本的對照一下
	
	kernel = tf.Variable(tf.truncated_normal([5, 5, 1, 64], dtype=tf.float32, stddev=1e-1, name="weights"))
	with tf.Session() as sess:

		sess.run(tf.global_variables_initializer())
		# print(sess.run(kernel))
		print(sess.run(o,feed_dict={tfinput:a}) )

		# coord = tf.train.Coordinator() #This class implements a simple mechanism to coordinate the termination of a set of threads.
		# threads = tf.train.start_queue_runners(coord=coord)
		# epoch = 1
		# try:
		# 	while not coord.should_stop():
		# 		# batch_data,batch_label = sess.run([x_batch,y_batch])
		# 		# print(type(batch_data),batch_data.shape)
		# 		# print(sess.run(loss,feed_dict={tfimage:batch_data,tflabel:batch_label}))

		# 		batch_data,batch_label = sess.run([x_batch,y_batch])
		# 		print("b_data,b_label: ",batch_data,batch_label)

		# 		batch_o = sess.run(o,feed_dict={tfinput:batch_data})
		# 		print("o: ",batch_o)
		# 		print(sess.run(loss,feed_dict={tfoutput:batch_o,tflabel:batch_label}))

		# 		print("epoch "+str(epoch))
		# 		if(epoch==10):
		# 			coord.request_stop() #Any of the threads can call coord.request_stop() to ask for all the threads to stop
		# 			coord.join(threads) #Wait for all the threads to terminate.
		# 		epoch+=1
		# finally:
		# 	coord.request_stop()
		# 	print("--Program end--")


	# y =[[1,0],[1,0],[0,1],[0,1],[0,1]]
	# path,label = tf.train.shuffle_batch([x,y],batch_size=3, capacity=10, min_after_dequeue=1,num_threads=1, allow_smaller_final_batch=False)
	# print(path)
	# print(label)
	# rand_index=np.arange(len(x))
	# print(rand_index)
	# random.shuffle(rand_index)
	# print(rand_index)

	# a = np.arange(9).reshape((1,3,3,1))
	# print(a)
	# print(a[0:])
	# tfoutput = tf.placeholder(dtype=tf.float32, shape=[None], name='input')

	# TEST about image
	# rand_image = tf.random_uniform([16,30,30,1], dtype=tf.float32)
	# rand_label = tf.random_uniform([16,3], dtype=tf.float32)	
	# tfimage = tf.placeholder(dtype=tf.float32, shape=[None,30,30,1], name='image')
	# tflabel = tf.placeholder(dtype=tf.float32, shape=[None,3], name='label')
	# x_batch,y_batch=tf.train.shuffle_batch([rand_image,rand_label],enqueue_many=True, batch_size=3, capacity=100, min_after_dequeue=10, allow_smaller_final_batch=True)
	# tfoutput = CNN(x_batch,0.5)
	# loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=tfoutput, labels=tflabel)) #問題就在這兩個參數

	# with tf.Session() as sess:
	# 	sess.run(tf.global_variables_initializer())
	# 	sess.run(tf.local_variables_initializer())
	# 	print("dd")
	# 	print(sess.run([path,label]))
	# 	print("dsd")

		
