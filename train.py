import tensorflow as tf
import os
import matplotlib.pyplot as plt
import numpy as np
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
from PIL import Image
import PIL.ImageOps 
model_path = "/model/saved_model.ckpt"
import random
# for debugging, outputs are Opetation name & Array size which you're using now.
def print_activations(t):
    print(t.op.name, " ", t.get_shape().as_list())

def debug(l,size):
    a=''
    fo = open("ex.txt", "w")
    for x in range(size):
        for i in range(300):
            for j in range(300):
                if l[x][i][j]==0.:
                    l[x][i][j]=0
                a=a+str(l[x][i][j])
                if j==299:
                    fo.write(a)
                    fo.write('\n')
                    a=''
        fo.write('\n'+'**********************'+'\n')

# During Training, CNN model's input would be batch Image(that is, x), at that moment, generate OneHotLabel(that is,y)
# Args: data is a full path. ex: data\shapeV\32033.png 
def getOneHot(data):
    catogories=3
    y=np.zeros([len(data),catogories])
    for i in range(len(data)):
        if data[i].find('shapeW')!=-1:
            y[i][0]=1
        elif data[i].find('shapeV')!=-1:
            y[i][1]=1
        else:
            y[i][2]=1
    return y

# Args: train for training data, test for testing data
# save data into an array
# Return: now is [pathlist], I'm considering return[pathlist,label]
def getData(arg):
    path_list=[]
    for root,dirs,files in os.walk(arg+'\\'):
        # print(root) #data\ or data\other
        for f in files:
            p = os.path.join(root,f)
            path_list.append(p)
            # print(os.path.join(root,f)) #data\other\32033.png
    return path_list

# Args: img_path is a path list of image. ex: ['data\other\32033.png','data\other\32533.png'...]
# Original image is 400x300, I'll resize it into 300x300
# Return: an nparray(300,300)
# About PIL library: https://blog.csdn.net/icamera0/article/details/50647465
# About PIL library: https://blog.csdn.net/majinlei121/article/details/78933947
def img_to_array(img_path):
    l = len(img_path) #l is 5, that is batch_size
    img_list=np.zeros((l,300,300,1))
    for i in range(len(img_path)):
        img = Image.open(img_path[i])
        img = img.resize((300,300),Image.BILINEAR)
        # imgplot = plt.imshow(img) # Watch out!! This is for debugging
        # plt.show()
        img = img.convert("L")
        img = PIL.ImageOps.invert(img) #White to Black (it means 255 to 0)
        img = np.array(img).reshape(300,300,1)
        img_list[i]=img
    img_list=img_list/255
    # print("img_list.shape: ",img_list.shape) # output:(len(training_data_path),300,300,1))
    #img_list is a tuple so not img_list.shape()[tuple can't call function, img_list.shape is an attribute]
    return img_list

# Args: data is image array([L,300,300,1]),L is image numbers; label is label array([L,3]);
# batch_size is size per batch;iter_num is how mant epochs you want, when reach iter_num, it would get OutOfRange Error
# Return: a batch. When calling batch.next() you can get batch.
def get_Batch(data, label, batch_size, iter_num):
    # print(data.shape, label.shape)
    input_queue = tf.train.slice_input_producer([data, label], num_epochs=iter_num, shuffle=True, capacity=32 ) 
    x_batch, y_batch = tf.train.batch(input_queue, batch_size=batch_size, num_threads=1, capacity=32, allow_smaller_final_batch=True)
    return x_batch, y_batch

#CNN
def CNN(image, keepprob=0.5):
    with tf.name_scope("conv1") as scope:
        kernel1 = tf.Variable(tf.truncated_normal([5, 5, 1, 32], dtype=tf.float32, stddev=1, name="weight1"))
        conv1 = tf.nn.conv2d(image, kernel1, [1, 1, 1, 1], padding="SAME")
        biases1 = tf.Variable(tf.constant(0.1, dtype=tf.float32, shape=[32]), trainable=True, name="bias1")
        bias1 = tf.nn.bias_add(conv1, biases1)
        conv1 = tf.nn.relu(bias1, name=scope)
    pool1 = tf.nn.max_pool(conv1,ksize=[1,10,10,1],strides=[1,3,3,1],padding="VALID",name="pool1")
    # print(pool1)#?, 97, 97, 32

    with tf.name_scope("conv2") as scope:
        kernel2 = tf.Variable(tf.truncated_normal([5, 5, 32, 64], dtype=tf.float32, stddev=0.1, name="weight2"))
        conv2 = tf.nn.conv2d(pool1, kernel2, [1, 1, 1, 1], padding="SAME")
        biases2 = tf.Variable(tf.constant(0.1, dtype=tf.float32, shape=[64]), trainable=True, name="bias2")
        bias2 = tf.nn.bias_add(conv2, biases2)
        conv2 = tf.nn.relu(bias2, name=scope)
    pool2 = tf.nn.max_pool(conv2,ksize=[1,10,10,1],strides=[1,3,3,1],padding="VALID",name="pool2")
    # print(pool2) #?, 30, 30, 64

    with tf.name_scope("dense") as scope:
        weight3 = tf.Variable(tf.truncated_normal([30*30*64,1024], dtype=tf.float32, stddev=0.1, name="weight3"))
        bias3 = tf.Variable(tf.constant(0.1, dtype=tf.float32, shape=[1024]), trainable=True, name="bias3")
        flatten1 = tf.reshape(pool2,[-1,30*30*64])
        fc1 = tf.nn.relu(tf.matmul(flatten1,weight3)+bias3)
        dropout1=tf.nn.dropout(fc1,keepprob)

    with tf.name_scope("output") as scope:
        weight4 = tf.Variable(tf.truncated_normal([1024,3], dtype=tf.float32, stddev=0.1, name="weight4"))
        bias4 = tf.Variable(tf.constant(0.1, dtype=tf.float32, shape=[3]), trainable=True, name="bias4")
        output = tf.matmul(dropout1,weight4) + bias4
        # output=tf.nn.softmax(tf.nn.dropout(output,keepprob))
        #不用加softmax 因為在 softmax_cross_entropy_with_logits 時就會將 output 做一次 softmax 了
        print(output)
    return output

def main():
    # Parameters
    iter_num=10  #iter num = ( data_size/batch_size ) * iter_num
    batch_size=10
    test_num=5
    lrn_rate=1e-3
    checkpoint=10
    # Get datas and data pre-processing
    training_data_path = getData('train')
    training_data_size = len(training_data_path) #150
    testing_data_path = getData('test')
    testing_data_size = len(testing_data_path) #40
    train_img = img_to_array(training_data_path)
    train_label=getOneHot(training_data_path)
    test_img = img_to_array(testing_data_path)
    test_label=getOneHot(testing_data_path)

    tfinput = tf.placeholder(dtype=tf.float32, shape=[None, 300, 300,1], name='input')
    tfoutput = tf.placeholder(dtype=tf.float32, shape=[None, 3], name='output')
    tflabel = tf.placeholder(dtype=tf.float32, shape=[None, 3], name='label')
    tfoutput = CNN(tfinput, 0.6)
    #kernel for debug

    x_batch,y_batch = get_Batch(train_img,train_label,batch_size,iter_num)
    tx_batch,ty_batch=get_Batch(test_img,test_label,batch_size,iter_num)

    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=tfoutput, labels=tflabel))
    # optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.09).minimize(loss)
    optimizer = tf.train.AdamOptimizer(learning_rate=lrn_rate).minimize(loss)
    accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(tfoutput, 1),tf.argmax(tflabel, 1)),tf.float32))
    tf.summary.scalar("Accuracy", accuracy)
    saver = tf.train.Saver()

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(sess, coord)
        epoch=1
        try:
            while not coord.should_stop():                
                batch_input,batch_label=sess.run([x_batch,y_batch])
                a, b = sess.run([optimizer, loss], feed_dict={tfinput: batch_input, tflabel: batch_label})
                print("Begin epoch: ",epoch," ,loss: ",b)
                # saver.save(sess, "model/model.ckpt",global_step=15,write_meta_graph=False )
                epoch+=1
                if epoch%checkpoint==0:
                    batch_t_input,batch_t_label=sess.run([tx_batch,ty_batch])
                    acc = accuracy.eval(feed_dict = {tfinput: batch_t_input, tflabel: batch_t_label})
                    print("step %d, training accuracy %g"%(epoch, acc))

        except tf.errors.OutOfRangeError:
            # out = sess.run(tfoutput,feed_dict={tfinput:train_img})
            # print(sess.run(tf.argmax(out,1)))
            # print(sess.run(tf.argmax(train_label,1)))
            # accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(out, 1),tf.argmax(train_label, 1)),tf.float32))
            # tf.summary.scalar("Accuracy", accuracy)
            # print(sess.run(accuracy))
            saver.save(sess, "model/model.ckpt")
            print("---end---")
        finally:
            coord.request_stop()
            print("--p end---")
        coord.join(threads)
        
        # can add this parameter:global_step=100 , write_meta_graph=False
        # save model per global_step

if __name__ == '__main__':
    main()