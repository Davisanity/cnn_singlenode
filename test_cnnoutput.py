import tensorflow as tf
import os
import matplotlib.pyplot as plt
import numpy as np
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
from PIL import Image
import PIL.ImageOps 
model_path = "/model/saved_model.ckpt"
import random
from tensorflow.examples.tutorials.mnist import input_data

def weight_variable(shape):
	initial = tf.truncated_normal(shape, stddev=0.1)
	return tf.Variable(initial)

def bias_variable(shape):
	initial = tf.constant(0.1, shape=shape)
	return tf.Variable(initial)
    
# 自訂 convolution 與 max-pooling 的函數
def conv2d(x, W):
	return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_2x2(x):
	return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

#CNN
def CNN(image, keepprob=0.5):
	# # 第一層是 Convolution 層（32 個神經元），會利用解析度 5x5 的 filter 取出 32 個特徵，然後將圖片降維成解析度 14x14
	# with tf.name_scope('FirstConvolutionLayer'):
	# 	W_conv1 = weight_variable([5, 5, 1, 32])
	# 	b_conv1 = bias_variable([32])
	# 	x_image = tf.reshape(image, [-1, 28, 28, 1])
	# 	h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)
	# 	h_pool1 = max_pool_2x2(h_conv1)

	# # 第二層是 Convolution 層（64 個神經元），會利用解析度 5x5 的 filter 取出 64 個特徵，然後將圖片降維成解析度 7x7
	# with tf.name_scope('SecondConvolutionLayer'):
	# 	W_conv2 = weight_variable([5, 5, 32, 64])
	# 	b_conv2 = bias_variable([64])
	# 	h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
	# 	h_pool2 = max_pool_2x2(h_conv2)

	# # 第三層是 Densely Connected 層（1024 個神經元），會將圖片的 1024 個特徵攤平
	# with tf.name_scope('DenselyConnectedLayer'):
	# 	W_fc1 = weight_variable([7 * 7 * 64, 1024])
	# 	b_fc1 = bias_variable([1024])
	# 	h_pool2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64])
	# 	h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

	# # 輸出結果之前使用 Dropout 函數避免過度配適
	# with tf.name_scope('Dropout'):
	# 	keep_prob = tf.placeholder(tf.float32)
	# 	h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

	# # 第四層是輸出層（10 個神經元），使用跟之前相同的 Softmax 函數輸出結果
	# with tf.name_scope('ReadoutLayer'):
	# 	W_fc2 = weight_variable([1024, 10])
	# 	b_fc2 = bias_variable([10])
	# 	y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2

	# return y_conv
###################################################################
	with tf.name_scope("conv1") as scope:
		kernel1 = tf.Variable(tf.truncated_normal([5, 5, 1, 32], dtype=tf.float32, stddev=0.1, name="weights"))
        # kernel1 = tf.Variable(tf.random_uniform([5, 5, 1, 32], dtype=tf.float32, minval=0,maxval=1, name="weights"))
		image = tf.reshape(image,[-1,28,28,1])
        # conv1 = tf.nn.conv2d(image, kernel1, [1, 1, 1, 1], padding="SAME")
		bias1 = tf.Variable(tf.constant(0.1, dtype=tf.float32, shape=[32]), trainable=True, name="biases")
        # bias1 = tf.nn.bias_add(conv1, biases1)
        # conv1 = tf.nn.relu(bias1, name=scope)
		conv1 = tf.nn.relu(conv2d(image,kernel1)+bias1)
    # pool1 = tf.nn.max_pool(conv1,ksize=[1,2,2,1],strides=[1,2,2,1],padding="VALID",name="pool1")
	pool1 = tf.nn.max_pool(conv1,ksize=[1,2,2,1],strides=[1,2,2,1],padding="SAME",name="pool1")
	# print(conv1)

	with tf.name_scope("conv2") as scope:
		kernel2 = tf.Variable(tf.truncated_normal([5, 5, 32, 64], dtype=tf.float32, stddev=0.1, name="weights"))
        # kernel2 = tf.Variable(tf.random_uniform([5, 5, 32, 64], dtype=tf.float32, minval=0,maxval=1, name="weights"))
        # conv2 = tf.nn.conv2d(pool1, kernel2, [1, 1, 1, 1], padding="SAME")
		bias2 = tf.Variable(tf.constant(0.1, dtype=tf.float32, shape=[64]), trainable=True, name="biases")
        # bias2 = tf.nn.bias_add(conv2, biases2)
        # conv2 = tf.nn.relu(bias2, name=scope)
		conv2 = tf.nn.relu(conv2d(pool1,kernel2)+bias2)
    # pool2 = tf.nn.max_pool(conv2,ksize=[1,2,2,1],strides=[1,2,2,1],padding="VALID",name="pool2")
	pool2 = tf.nn.max_pool(conv2,ksize=[1,2,2,1],strides=[1,2,2,1],padding="SAME",name="pool2")
	print(pool2) #(?,7,7,64)
    # flatten layer without BIAS
	with tf.name_scope("dense") as scope:
		weight3 = tf.Variable(tf.truncated_normal([7*7*64,1024], dtype=tf.float32, stddev=0.1, name="weights3"))
		bias3 = tf.Variable(tf.constant(0.1, dtype=tf.float32, shape=[1024]), trainable=True, name="biases")
		flatten1 = tf.reshape(pool2,[-1,7*7*64])
		fc1 = tf.nn.relu(tf.matmul(flatten1,weight3)+bias3)
	    # weight1 =tf.Variable(tf.truncated_normal([7*7*64,10], dtype=tf.float32, stddev=1e-1, name="weights"))
	    # fc1 = tf.nn.relu(tf.matmul(flatten1,weight1)) 
		dropout1=tf.nn.dropout(fc1,keepprob)

	with tf.name_scope("output") as scope:
		weight4 = tf.Variable(tf.truncated_normal([1024,10], dtype=tf.float32, stddev=0.1, name="weights4"))
		bias4 = tf.Variable(tf.constant(0.1, dtype=tf.float32, shape=[10]), trainable=True, name="biases")
		output = tf.matmul(dropout1,weight4) + bias4

	return output

def debug(l,size):
    a=''
    r=28
    fo = open("mnist.txt", "w")
    for x in range(size):
        for i in range(r):
            for j in range(r):
                if l[x][i][j]==0:
                    l[x][i][j]=0
                else:
                	l[x][i][j]="%.1f" % l[x][i][j] 
                a=a+str(l[x][i][j])
                if j==r-1:
                    fo.write(a)
                    fo.write('\n')
                    a=''
        fo.write('\n'+'**********************'+'\n')

def batch(i,b,img,label,indices):
    batch_x = []
    batch_y = []
    for j in range(b):
        # print(i,j)
        batch_x.append(img[indices[i+j]])
        batch_y.append(label[indices[i+j]])
    # print(len(batch_x))
    return batch_x,batch_y

def next_batch(num, data, labels):
    idx = np.arange(0 , len(data))
    np.random.shuffle(idx)
    idx = idx[:num]
    data_shuffle = [data[ i] for i in idx]
    labels_shuffle = [labels[ i] for i in idx]
    return np.asarray(data_shuffle), np.asarray(labels_shuffle)

def get_Batch(data, label, batch_size):
	# print(data.shape, label.shape)
	input_queue = tf.train.slice_input_producer([data, label], num_epochs=10, shuffle=True, capacity=32 ) 
	x_batch, y_batch = tf.train.batch(input_queue, batch_size=batch_size, num_threads=1, capacity=32, allow_smaller_final_batch=True)
	return x_batch, y_batch


if __name__ == '__main__':
	mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)
	train_img = mnist.train.images
	train_label = mnist.train.labels
	img_list = []

	train_size=1500
	test_size=200

	img_list=[]
	label_list = train_label[0:train_size]
	_label = np.asarray(label_list)
	# print(type(label_list))
	for i in range(train_size):
		curr_img   = np.reshape(train_img[i, :], (28, 28 ,1)) # 28 by 28 matrix 
		img_list.append(curr_img)
	_img = np.asarray(img_list)

	test_img = mnist.test.images
	test_label = mnist.test.labels

	timg_list=[]
	tlabel_list=test_label[0:test_size]
	_tlabel=np.asarray(tlabel_list)
	for i in range(test_size):
		c = np.reshape(test_img[i,:],(28,28,1))
		timg_list.append(c)
	_timg = np.asarray(timg_list)

	batch_size=50
	num_iters=1000 #在get_batch的num_epochs參數裡了
	lrn_rate=1e-4
	test_num=50
	checkpoint=50

	tfinput = tf.placeholder(dtype=tf.float32, shape=[None, 28, 28,1], name='input')
	tfoutput = tf.placeholder(dtype=tf.float32, shape=[None, 10], name='output')
	tflabel = tf.placeholder(dtype=tf.float32, shape=[None, 10], name='label')
	tfoutput = CNN(tfinput, 0.6) #HERE can do some normalize

	x_batch,y_batch = get_Batch(_img,_label,batch_size)
	tx_batch,ty_batch=get_Batch(_timg,_tlabel,batch_size)

	loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=tfoutput, labels=tflabel))
	optimizer = tf.train.AdamOptimizer(learning_rate=lrn_rate).minimize(loss)
	accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(tfoutput, 1),tf.argmax(tflabel, 1)),tf.float32))
	tf.summary.scalar("Accuracy", accuracy)

	with tf.Session() as sess:
		sess.run(tf.global_variables_initializer())
		sess.run(tf.local_variables_initializer())
		coord = tf.train.Coordinator()
		threads = tf.train.start_queue_runners(sess, coord)
		epoch=0

		try:
			while not coord.should_stop():
				# print("Begin epoch: ",epoch)
				batch_input,batch_label=sess.run([x_batch,y_batch])
				a, b = sess.run([optimizer, loss], feed_dict={tfinput: batch_input, tflabel: batch_label})
				epoch+=1
				if epoch%checkpoint==0:
					batch_t_input,batch_t_label=sess.run([tx_batch,ty_batch])
					acc = accuracy.eval(feed_dict = {tfinput: batch_t_input, tflabel: batch_t_label})
					print("step %d, training accuracy %g"%(epoch, acc))
			
		except tf.errors.OutOfRangeError:
			print("---end---")
		finally:
			coord.request_stop()
			print("--p end---")
		coord.join(threads)


		# for epoch in range(num_iters):
		# 	print("Begin epoch: ",epoch)
		# 	training_indices = np.arange(train_size)     #[ 1 2 3 4 5]
		# 	random.shuffle(training_indices)      		 #[ 2 3 4 5 1]
		# 	testing_indices = np.arange(test_size)
		# 	random.shuffle(testing_indices) 
		# 	batch_input,batch_label=batch(i,batch_size,img_list,label_list,training_indices)
  #           #研究一下batch!!!!!!!###########
		# 	# batch_input,batch_label = next_batch(batch_size,img_list,label_list)
  #           ###############################
  #           #每一次丟進CNN的都是 batch_size 張圖片 每 epoch 都做 size/batch_size 次運算
		# 	for i in range(0,train_size,batch_size):
				
  #               # debug(batch_input,10)
		# 		# print("output: \n", sess.run(tfoutput,feed_dict={tfinput:batch_input}) )
  #               # print("kernel: ", sess.run(kernel,feed_dict={tfinput:batch_input}) )
  #               # print("label:  \n",batch_label)
		# 		# print(batch_label)#  一堆50 不太對近
		# 		a, b = sess.run([optimizer, loss], feed_dict={tfinput: batch_input, tflabel: batch_label})
		# 		# print("optimizer: ",a," ,loss: ",b)

		# 	if epoch%checkpoint==0: 
		# 		acc = accuracy.eval(feed_dict = {tfinput: batch_input, tflabel: batch_label})
		# 		print("step %d, training accuracy %g"%(epoch, acc))
		# 		# for x in range(0,test_size,test_num): 
		# 			# acc_input,acc_label=batch(x,test_num,timg_list,tlabel_list,testing_indices)
  #                   # acc_input,acc_label=batch(x,test_num,training_data_path,training_indices)              		
		# 		# 	acc+=sess.run(accuracy,feed_dict={tfinput:acc_input,tflabel:acc_label})
		# 		# acc=acc/test_num

		# 		# print("In epoch ",epoch," accurate is: ",acc)
