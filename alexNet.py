import tensorflow as tf
import os
import matplotlib.pyplot as plt
import numpy as np
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
from PIL import Image
model_path = "/model/saved_model.ckpt"
batch_size = 5
num_iters = 3

# During Training, CNN model's input would be batch Image(that is, x), at that moment, generate OneHotLabel(that is,y)
# Args: data is a full path. ex: data\shapeV\32033.png 
def getOneHot(data):
	catogories=3
	y=np.zeros([len(data),catogories])
	for i in range(len(data)):
		if data[i].find('shapeW')!=-1:
			y[i][0]=1
		elif data[i].find('shapeV')!=-1:
			y[i][1]=1
		else:
			y[i][2]=1
	# print(y)
	return y
# get training data and labeled by One-Hot-Code
# save training data into an array
# Return: now is [pathlist], I'm considering return[pathlist,label]
def getData():
	path_list=[]
	for root,dirs,files in os.walk('t_data\\'):
		# print(root) #data\ or data\other
		for f in files:
			p = os.path.join(root,f)
			path_list.append(p)
			# print(os.path.join(root,f)) #data\other\32033.png
			# img_to_array(p)
	# print(path_list)
	return path_list

# Args: img_path is a full path list of image. ex: ['data\other\32033.png','data\other\32533.png'...]
# Original image is 400x300, I'll resize it into 300x300
# Return: an nparray(300,300)
# About PIL library: https://blog.csdn.net/icamera0/article/details/50647465
# Abour PIL library: https://blog.csdn.net/majinlei121/article/details/78933947
# 使用時機: 在batch決定要拿哪些進去訓練時，要把path的Image變成Array
def img_to_array(img_path):
	l = len(img_path) #l is 5, that is batch_size
	img_list=np.zeros((l,300,300,1))
	for i in range(len(img_path)):
		img = Image.open(img_path[i])
		img = img.resize((300,300),Image.BILINEAR)
		# imgplot = plt.imshow(img) # Watch out!! This is for debugging
		# plt.show()
		img = img.convert("L")
		img = np.array(img).reshape(300,300,1)
		img_list[i]=img
	# print("img_list.shape: ",img_list.shape) # output:(len(img_path),300,300,1))
	#img_list is a tuple so not img_list.shape()[tuple can't call function, img_list.shape is an attribute]
	return img_list	

# https://blog.csdn.net/sinat_35821976/article/details/82668555
# https://dotblogs.com.tw/shaynling/2018/04/26/164331
def batch(x,y):
	b_x,b_y = tf.train.shuffle_batch([x,y],enqueue_many=True, batch_size=batch_size, capacity=100, min_after_dequeue=10, allow_smaller_final_batch=True)
	# REF: https://www.tensorflow.org/api_docs/python/tf/train/shuffle_batch
	# print(b_x,b_y) #Tensor("shuffle_batch:0", shape=(?,), dtype=string) Tensor("shuffle_batch:1", shape=(?, 3), dtype=float64)
	return b_x,b_y

# for debugging, outputs are Opetation name & Array size which you're using now.
def print_activations(t):
    print(t.op.name, " ", t.get_shape().as_list())

''' construct AlexNet model. Some of the kernels and structure copyed by AlexNet official website
# https://github.com/tensorflow/models/blob/master/tutorials/image/alexnet/alexnet_benchmark.py
# https://blog.csdn.net/DaVinciL/article/details/76301820
# Args: image is an image array
'''
#ValueError: Shapes must be equal rank, but are 4 and 1
# ut shapes: [5,5,1,64], [64]
#CNN functions
#好神奇 不能用呼叫這些func的方法 會出現error
def kernel_weight(shape):
	kernel = tf.Variable(tf.truncated_normal(shape,stddev=0.1,dtype=tf.float32,name="weights")) #truncated_normal(shape)
	return tf.Variable(kernel)
def bias_variable(shape):
	biases = tf.Variable(tf.constant(0.0, dtype=tf.float32, shape=shape), trainable=True, name="biases")
	return tf.Variable(biases)
def conv2d(x,w,stride):
	return tf.nn.conv2d(x,w,strides=stride,padding="SAME") #Args: input,filter,strides([1,height,width,1]),padding

def CNN(image,keepprob=0.5):
	parameters = []
	with tf.name_scope("conv1") as scope:
		# kernel = kernel_weight([5,5,1,64]) # patch 5x5, channel size 1, out size 64
		# # conv1/Variable_1   [5, 5, 1, 64] -> FailedPreconditionError: Attempting to use uninitialized value conv1/Variable_1
		# biases = bias_variable([64])
		# bias = tf.nn.bias_add(conv2d(image,kernel,[1,5,5,1]),biases)
		# conv1 = tf.nn.relu(bias, name=scope)
		kernel = tf.Variable(tf.truncated_normal([5, 5, 1, 64], dtype=tf.float32, stddev=1e-1, name="weights"))   ########為什麼要用variable 感覺可以不用??????
		conv = tf.nn.conv2d(image, kernel, [1, 5, 5, 1], padding="SAME")
		biases = tf.Variable(tf.constant(0.0, dtype=tf.float32, shape=[64]), trainable=True, name="biases")       ########參考cnnfoeww.py
		bias = tf.nn.bias_add(conv, biases)
		conv1 = tf.nn.relu(bias, name=scope)
		# print_activations(conv1)
		parameters+=[kernel,biases]
	pool1 = tf.nn.max_pool(conv1,ksize=[1,3,3,1],strides=[1,2,2,1],padding="VALID",name="pool1")
	# print_activations(pool1)
	# flatten layer 
	#尚未加入 bias
	flatten1 = tf.reshape(pool1,[-1,29*29*64])
	weight1 = kernel_weight([29*29*64,3])
	fc1 = tf.nn.relu(tf.matmul(flatten1,weight1)) 
	dropout1=tf.nn.dropout(fc1,keepprob)
	# print_activations(dropout1)
	return dropout1

def main():
	img_path = getData()
	label = getOneHot(img_path)
	# img_path = img_to_array(img_path) #嘗試在這裡就轉換成array
	tfinput = tf.placeholder(dtype=tf.float32, shape=[None, 300, 300,1], name='input')
	tfoutput = tf.placeholder(dtype=tf.float32, shape=[None, 3], name='output')
	tflabel = tf.placeholder(dtype=tf.float32, shape=[None, 3], name='label')
	x_batch,y_batch=batch(img_path,label)
	# tfinput=tf.reshape(tfimage,[-1,300,300,1])
	cnn_output=CNN(tfinput,0.5)
	# print("cnn_output:",cnn_output)
	loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=tfoutput, labels=tflabel))
	# optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.09).minimize(loss) #can try AdamOptimizer
	# #Define accuracy
	# accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(tfoutput,1),tf.argmax(tflabel,1)),tf.float32))
	# module_file =  tf.train.latest_checkpoint('/model/saved_model.ckpt')
	# saver = tf.train.Saver()

	with tf.Session() as sess:
		# sess.run(tf.local_variables_initializer())
		sess.run(tf.global_variables_initializer())  
		coord = tf.train.Coordinator() #This class implements a simple mechanism to coordinate the termination of a set of threads.
		threads = tf.train.start_queue_runners(coord=coord)
		for i in range(10):
			print("epoch: ",i)
			batch_data,batch_label = sess.run([x_batch,y_batch])
			batch_data = img_to_array(batch_data)
			print(batch_data.shape)
			batch_output=sess.run(cnn_output,feed_dict={tfinput:batch_data})
				# print(batch_data.shape) #output: (batch_size,300,300)
				# a,b=sess.run([optimizer,loss],feed_dict={tfimage:batch_data,tflabel:batch_label})
			print( sess.run(loss,feed_dict={tfoutput:batch_output,tflabel:batch_label}) )
				# print("sessrum:",sess.run(tfoutput,feed_dict={tfimage:batch_data}))
				### raise type(e)(node_def, op, message)
				
			if(i==num_iters):
				coord.request_stop() #Any of the threads can call coord.request_stop() to ask for all the threads to stop
				coord.join(threads) #Wait for all the threads to terminate.
		
		# except tf.errors.OutOfRangeError:
		# 	print("--Finish Training--")
		# finally:
		coord.request_stop()
		coord.join(threads)
		# 	# saver.save(sess, model_path)
		# 	print("--Program end--")
			
		

if __name__ == "__main__":
    main()
    # p = getData()
    # img_to_array(p)