from PIL import Image
import matplotlib.pyplot as plt
from PIL import Image
import PIL.ImageOps 
import numpy as np
path = "train\\shapeW\\88329.png"

s = 300
img = Image.open(path)

img = img.resize((s,s),Image.BILINEAR)

imgplot = plt.imshow(img)

gimg = img.convert("L")  

inverted_image = PIL.ImageOps.invert(gimg)
inverted_image.save('new_name.png')

# gimg.save("g.png")
# imgplot = plt.imshow(img) # Watch out!! This is for debugging
# plt.show(inverted_image)

# img2 = np.array(gimg).reshape(s,s,1)
img2 = np.array(inverted_image).reshape(s,s,1)
img2 = img2/255
a=''
fo = open("image2.txt", "w")
for i in range(s):
	for j in range(s):
		if img2[i][j]==0.:
			img2[i][j]=int(0)

		a=a+str(img2[i][j])
		if j==s-1:
			fo.write(a)
			fo.write('\n')
			a=''

# convert_img= Image.fromarray(img)
# imgplot2 = plt.imshow(convert_img) # Watch out!! This is for debugging
plt.show()

fo.close()