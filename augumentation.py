from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
import os
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np
import glob
import scipy.ndimage

name = []
test = "test\\"
w = "w\\"
v = "v\\"
genV = "genV"
genW = "genW"

epoch = 4000
batch_size = 1

def print_result(path):
    name_list = glob.glob(path)
    fig = plt.figure()
    for i in range(9):
        img = Image.open(name_list[i])
        # add_subplot(331) 参数一：子图总行数，参数二：子图总列数，参数三：子图位置
        sub_img = fig.add_subplot(331 + i)
        sub_img.set_xticks([])  
        sub_img.set_yticks([]) 
        sub_img.imshow(img)
    plt.show()
    return fig

#Zoom in (Big)
datagen = ImageDataGenerator(
    zca_whitening=False,
    rotation_range=20,
    width_shift_range=30,
    height_shift_range=30,
    shear_range=0.5,
    zoom_range=0.2,
    horizontal_flip=False,
    vertical_flip=False,
    fill_mode='nearest')

#Zoom out (Small)
# 效果不好
# datagen2 = ImageDataGenerator(
#     zca_whitening=False,
#     rotation_range=20,
#     width_shift_range=30,
#     height_shift_range=30,
#     shear_range=0.5,
#     zoom_range=4,
#     horizontal_flip=False,
#     vertical_flip=False,
#     fill_mode='nearest')

##################
# Single SHOW
# img = load_img("test\\shapeV\\02662.png")
# y = img_to_array(img)
# l=[]
# l.append(y)
# l = np.array(l)
# i = 1
# for batch in datagen.flow(l, batch_size=1,save_to_dir=gen_dir, save_prefix='v', save_format='png'):
#     i+=1
#     if i > epochs:
#         break
# print_result(gen_dir+ '*\*')

##################
#因為這是要拿來訓練的所以直接300x300
#這個是要到子目錄的上一層 ex test 下面有 v w o 他就會說 Found 42 images belonging to 3 classes.
gen_dataV = datagen.flow_from_directory(v,target_size=(300,300),batch_size=batch_size,color_mode='grayscale',save_to_dir=genV,save_prefix='v')
for i in range(epoch):
  gen_dataV.next()

gen_dataW = datagen.flow_from_directory(w,target_size=(300,300),batch_size=batch_size,color_mode='grayscale',save_to_dir=genW,save_prefix='v')
for i in range(epoch):
    gen_dataW.next()