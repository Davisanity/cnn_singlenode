import tensorflow as tf
import os
import numpy as np
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

def get_Batch(data, label, batch_size):
	# print(data.shape, label.shape)
	input_queue = tf.train.slice_input_producer([data, label], num_epochs=1, shuffle=True, capacity=32 ) 
	x_batch, y_batch = tf.train.batch(input_queue, batch_size=batch_size, num_threads=1, capacity=32, allow_smaller_final_batch=True)
	return x_batch, y_batch


if __name__ == "__main__":
	#a = (7,3,3)
	# b=(7,3)
	a=np.arange(0,63).reshape(7,3,3)
	b=np.arange(0,21).reshape(7,3)
	aa,ab=get_Batch(a,b,3)
with tf.Session() as sess:
	sess.run(tf.global_variables_initializer())
	sess.run(tf.local_variables_initializer())
	coord = tf.train.Coordinator()
	threads = tf.train.start_queue_runners(sess, coord)
	epoch=0
	try:
		while not coord.should_stop():
			print("epoch ",epoch)
			ba,bb=sess.run([aa,ab])
			epoch+=1
			print("a:",ba)
			print("b:",bb)
	except tf.errors.OutOfRangeError:
		print("---end---")
	finally:
		coord.request_stop()
		print("--p end---")
	coord.join(threads)